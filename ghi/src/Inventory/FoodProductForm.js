 import React from "react";
 import { useState } from "react";

 function FoodProductForm(props) {
    const [main_ingredient, setMainIngredient] = useState('');

    return (
        <form>
            <div className="mb-3">
                <label htmlFor="ingredients" className="form-label">Main Ingredient</label>
                <input value={main_ingredient} required type="text" className="form-control" id="ingredients" placeholder="Chicken"/>
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
    );
 }

 export default FoodProductForm
